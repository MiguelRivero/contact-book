/**
 * Created by Miguel Rivero on 06/11/2014.
 * Last update on 08/12/2014
 */

var contact_controller = angular.module('contact_controller',[]);

contact_controller.controller("datos_ctlr",['$scope', function($scope){
    // datos must be ordered by id, otherwise new_id() will fail.
    var defaultContacts = [
        {id:1, name:'Alexandra Collingwood', email: 'alex@hotmail.com', phone: '38937239387', country: 'Great Britain', favourite: false},
        {id:2, name:'Rebecca Schnitzler', email: 'rebecca@hotmail.com', phone: '32094384723', country: 'Germany', favourite: false},
        {id:3, name:'Claude Pierre', email: 'claudepierre@hotmail.com', phone:'73293802398', country: 'France', favourite: false},
        {id:4, name:'Matthew Dąbrowski', email: 'mat@gmail.com', phone: '21298389703', country: 'Poland', favourite: false},
        {id:5, name:'Miguel Rivero', email: 'rmiguelrivero@gmail.com', phone:'07477922001', country: 'Spain', favourite: true},
        {id:6, name:'John Doe', email: 'jdoe@outlook.com', phone: '09122891238', country: 'United States', favourite: false}
        ];

    $scope.contacts = JSON.parse( localStorage.contacts || null ) || angular.copy(defaultContacts);

    // $scope.countries = ['Great Britain', 'Germany', 'Frence', 'Poland', 'Spain', 'United States'];

    $scope.predicate = 'name';
    // if edit = true means you are editing a contact
    $scope.edit = false;
    // empty keeps the track whether the contact list is empty or not
    $scope.empty = false;


    $scope.add = function(person) {
        var index2;
        var exist = false;
        for (var index = 0; index < $scope.contacts.length; index ++){
            if ($scope.contacts[index].id == person.id){
                index2 = index;
                exist = true;
            }
        }
        if($scope.edit){
            // edit an  existing contact
            var copy_person = angular.copy(person);
            $scope.contacts[index2] = copy_person;
            $scope.edit = false;
            reset();
        }else{
            // add new contact
            if (exist == true){
                // if you try to add contact with same name as an existing contact
                console.log("This person is already in the list.");
                alert("This person is already in the list.");
            }else{
                person.id = new_Id();
                var copy_person = angular.copy(person);
                $scope.contacts.push(copy_person);
                console.log(copy_person.id);
                reset();
            }
        }
    };

    function new_Id(){
        // Last object in datos contains the highest index
        var id = 1;
        if ( $scope.contacts != undefined && $scope.contacts.length > 0) {
            id =  $scope.contacts[$scope.contacts.length - 1].id + 1;
        }
        return id;
    }

    function reset() {
        // Restore inputs to undefined
        $scope.nperson = {};
        // Hide modal
        $('#EditModal').modal('hide');
        save();
        // Set empty to false in case dato is not empty
        if ($scope.contacts.length > 0){
            $scope.empty = false;
        }
    }

    function save(){
        // Storage datos in localstorage
        localStorage.contacts = JSON.stringify($scope.contacts);
    }

    $scope.remove_contact = function(id){
        var index_to_remove;
        for (var index = 0; index < $scope.contacts.length; index ++){
            if ($scope.contacts[index].id == id){
                index_to_remove = index;
            }
        }
        $scope.contacts.splice(index_to_remove, 1);
        if($scope.contacts == undefined || $scope.contacts.length == 0){
            $scope.empty = true;
        }
        save();
    };

    $scope.edit_contact = function(id){
        $scope.nperson = {};
        $('#EditModal').show();
        var index_to_edit;
        for (var index = 0; index < $scope.contacts.length; index ++){
            if ($scope.contacts[index].id == id){
                index_to_edit = index;
            }
        }
        // Fill the inputs with contact info
        $scope.nperson.id = $scope.contacts[index_to_edit].id;
        $scope.nperson.name = $scope.contacts[index_to_edit].name;
        $scope.nperson.email = $scope.contacts[index_to_edit].email;
        $scope.nperson.phone = $scope.contacts[index_to_edit].phone;
        $scope.nperson.favourite = $scope.contacts[index_to_edit].favourite;
        $scope.nperson.country = $scope.contacts[index_to_edit].country;
        // Set edit status to true
        $scope.edit = true;
    };

    $scope.cancel = function(){
        $scope.edit = false;
        $scope.nperson = {};
        reset();
    };

    $scope.clearList = function() {
        $scope.contacts = [];
        $scope.empty = true;
    };

    $scope.loadTestData = function() {
        $scope.empty = false;
        $scope.contacts = angular.copy(defaultContacts);
        save();
    }

    $scope.countries = {
        AFG: "Afghanistan", ALB: "Albania", ALG: "Algeria", AND: "Andorra", ANG: "Angola",
        ANT: "Antigua and Barbuda", ARG: "Argentina", ARM: "Armenia", ARU: "Aruba",
        ASA: "American Samoa", AUS: "Australia", AUT: "Austria", AZE: "Azerbaijan",
        BAH: "Bahamas", BAN: "Bangladesh", BAR: "Barbados", BDI: "Burundi", BEL: "Belgium",
        BEN: "Benin", BER: "Bermuda", BHU: "Bhutan", BIH: "Bosnia and Herzegovina", BIZ: "Belize",
        BLR: "Belarus", BOL: "Bolivia", BOT: "Botswana", BRA: "Brazil", BRN: "Bahrain",
        BRU: "Brunei", BUL: "Bulgaria", BUR: "Burkina Faso", CAF: "Central African Republic",
        CAM: "Cambodia", CAN: "Canada", CAY: "Cayman Islands", CGO: "Congo", CHA: "Chad",
        CHI: "Chile", CHN: "China", CIV: "Cote d'Ivoire", CMR: "Cameroon", COD: "DR Congo",
        COK: "Cook Islands", COL: "Colombia", COM: "Comoros", CPV: "Cape Verde", CRC: "Costa Rica",
        CRO: "Croatia", CUB: "Cuba", CYP: "Cyprus", CZE: "Czech Republic", DEN: "Denmark",
        DJI: "Djibouti", DMA: "Dominica", DOM: "Dominican Republic", ECU: "Ecuador",
        EGY: "Egypt", ERI: "Eritrea", ESA: "El Salvador", ESP: "Spain", EST: "Estonia",
        ETH: "Ethiopia", FIJ: "Fiji", FIN: "Finland", FRA: "France", FSM: "Micronesia",
        GAB: "Gabon", GAM: "Gambia", GBR: "Great Britain", GBS: "Guinea-Bissau", GEO: "Georgia",
        GEQ: "Equatorial Guinea", GER: "Germany", GHA: "Ghana", GRE: "Greece", GRN: "Grenada",
        GUA: "Guatemala", GUI: "Guinea", GUM: "Guam", GUY: "Guyana", HAI: "Haiti",
        HKG: "Hong Kong", HON: "Honduras", HUN: "Hungary", INA: "Indonesia", IND: "India",
        IRI: "Iran", IRL: "Ireland", IRQ: "Iraq", ISL: "Iceland", ISR: "Israel",
        ISV: "Virgin Islands", ITA: "Italy", IVB: "British Virgin Islands", JAM: "Jamaica",
        JOR: "Jordan", JPN: "Japan", KAZ: "Kazakhstan", KEN: "Kenya", KGZ: "Kyrgyzstan",
        KIR: "Kiribati", KOR: "South Korea", KSA: "Saudi Arabia", KUW: "Kuwait", LAO: "Laos",
        LAT: "Latvia", LBA: "Libya", LBR: "Liberia", LCA: "Saint Lucia", LES: "Lesotho",
        LIB: "Lebanon", LIE: "Liechtenstein", LTU: "Lithuania", LUX: "Luxembourg",
        MAD: "Madagascar", MAR: "Morocco", MAS: "Malaysia", MAW: "Malawi", MDA: "Moldova",
        MDV: "Maldives", MEX: "Mexico", MGL: "Mongolia", MHL: "Marshall Islands", MKD: "Macedonia",
        MLI: "Mali", MLT: "Malta", MNE: "Montenegro", MON: "Monaco", MOZ: "Mozambique",
        MRI: "Mauritius", MTN: "Mauritania", MYA: "Myanmar", NAM: "Namibia", NCA: "Nicaragua",
        NED: "Netherlands", NEP: "Nepal", NGR: "Nigeria", NIG: "Niger", NOR: "Norway",
        NRU: "Nauru", NZL: "New Zealand", OMA: "Oman", PAK: "Pakistan", PAN: "Panama",
        PAR: "Paraguay", PER: "Peru", PHI: "Philippines", PLE: "Palestine", PLW: "Palau",
        PNG: "Papua New Guinea", POL: "Poland", POR: "Portugal", PRK: "North Korea",
        PUR: "Puerto Rico", QAT: "Qatar", ROU: "Romania", RSA: "South Africa", RUS: "Russia",
        RWA: "Rwanda", SAM: "Samoa", SEN: "Senegal", SEY: "Seychelles", SIN: "Singapore",
        SKN: "Saint Kitts and Nevis", SLE: "Sierra Leone", SLO: "Slovenia", SMR: "San Marino",
        SOL: "Solomon Islands", SOM: "Somalia", SRB: "Serbia", SRI: "Sri Lanka",
        STP: "Sao Tome and Principe", SUD: "Sudan", SUI: "Switzerland", SUR: "Suriname",
        SVK: "Slovakia", SWE: "Sweden", SWZ: "Swaziland", SYR: "Syria", TAN: "Tanzania",
        TGA: "Tonga", THA: "Thailand", TJA: "Tajikistan", TKM: "Turkmenistan",
        TLS: "Timor-Leste", TOG: "Togo", TPE: "Chinese Taipei", TRI: "Trinidad and Tobago",
        TUN: "Tunisia", TUR: "Turkey", TUV: "Tuvalu", UAE: "United Arab Emirates",
        UGA: "Uganda", UKR: "Ukraine", URU: "Uruguay", USA: "United States",
        UZB: "Uzbekistan", VAN: "Vanuatu", VEN: "Venezuela", VIE: "Vietnam",
        VIN: "Saint Vincent and the Grenadines", YEM: "Yemen", ZAM: "Zambia", ZIM: "Zimbabwe"
    };

}]);