# Contacts Book #

It's a basic contacts manager build in javascript with jQuery and AngularJS.

### What info does it save? ###

* Full Name
* Email
* Telephone
* Country
* Favourite

### Live demo ###

LocalStorage Backend - [miguelrivero.info/contacts_book](http://miguelrivero.info/contacts_book/)

Google AppEngine API Backend - [miguelrivero.info/contacts_book_api](http://miguelrivero.info/contacts_book_api/) (12/01/2015)

### Update ###

9/01/2015

* Modal's behaviour changed:
    * Prevent hiding on click outside modal window
    * Prevent hiding on press "Esc"
    * Cleaning inputs on click close button

8/12/2014

* persistence with localStorage
* Added group buttons with new options
    * Clear list button
    * Load test contacts button.

### Images ###

![contacts.PNG](https://bitbucket.org/repo/R856zb/images/2460197688-contacts.PNG)

![contacts2.PNG](https://bitbucket.org/repo/R856zb/images/1576228217-contacts2.PNG)